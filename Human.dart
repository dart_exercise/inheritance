//Inheritance
class Human {
  late String name;
  late int age;

  void introduce() {
    print("My name is " + this.name + ". age $age.");
  }
}

//Type Jobs
//Artist
mixin Artist {
  late String name;
  void playMusic() {
    print(this.name + " can play music.");
  }

  void introduce() {
    print("My name is " + this.name + ". I'm Artist.");
  }
}
//Actress
mixin Actress {
  late String name;
  void castActing() {
    print(this.name + " can acting.");
  }

  void introduce() {
    print("My name is " + this.name + ". I'm actress.");
  }
}
//Programmer
mixin Programmer {
  late String name;
  void writeCode() {
    print(this.name + " can write code.");
  }

  void introduce() {
    print("My name is " + this.name + ". I'm Programmer.");
  }
}

//void main
void main(List<String> args) {
  //James
  var james = James();
  james.introduce();
  james.playMusic();
  print(" ");
  //Clem
  var clem = Clem();
  clem.introduce();
  clem.castActing();
  print(" ");
  //Wip
  var wip = Wip();
  wip.introduce();
  wip.writeCode();
  print(" ");
  //Venus
  var venus = Venus();
  venus.introduce();
}

class James extends Human with Artist {
  @override
  String name = "James";
  int age = 26;
}

class Clem extends Human with Actress {
  @override
  String name = "Clementine";
  int age = 18;
}

class Wip extends Human with Programmer {
  @override
  String name = "Wip";
  int age = 19;
}

class Venus extends Human {
  @override
  String name = "Venus";
  int age = 21;
}
